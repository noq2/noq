package com.example.noq

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.noq.AssistingFunctions.totalAmountStr
import com.example.noq.AssistingFunctions.totalAmountVariable
import kotlinx.android.synthetic.main.product_item.view.*


class ProductListAdapter(
    private val barcodeLayout: RelativeLayout,
    private val c: Context,
    private val products: ArrayList<ProductModel>,
    private val totalAmountBarcodePage: TextView,
    private var cantDeleteWhenPopupWindowFlag: Int = 0
) :
    RecyclerView.Adapter<ProductListAdapter.ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder =
        ProductHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        )

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val product = products[position]
        val productPrice = product.price
        var productAmount = product.purchase_counter
        var totalPriceToShow = (productPrice * productAmount)

        // get the TextView of total amount from BarcodeScannerPage
        holder.itemView.product_title.text = product.title
        holder.itemView.product_price.text =
            totalPriceToShow.toString()
        holder.itemView.purchase_counter.text = "${product.purchase_counter}"

        holder.itemView.details_menu_btn.setOnClickListener {
            cantDeleteWhenPopupWindowFlag = 1 //for denny delete product later

            // set pop up view
            val window = PopupWindow(this.c)

            // set pop up size
            window.width = 700;
            window.height = 300;

            val view = LayoutInflater.from(this.c).inflate(R.layout.details_menu, null)
            window.contentView = view

            val popupDetailsMenu = view.findViewById<ImageButton>(R.id.close_btn)

            view.findViewById<TextView>(R.id.product_title).text = product.title
            view.findViewById<TextView>(R.id.product_type).text = product.type
            view.findViewById<TextView>(R.id.product_subType).text = product.subType
            view.findViewById<TextView>(R.id.product_size).text = product.size
            view.findViewById<TextView>(R.id.product_color).text = product.color

            // dismiss details window when pressing entire background
            barcodeLayout.setOnClickListener {
                window.dismiss()
            }

            // dismiss details window when pressing the X button
            popupDetailsMenu.setOnClickListener {
                window.dismiss()
            }

            // show the menu
            window.showAsDropDown(holder.itemView.details_menu_btn)
        }

        holder.itemView.product_delete_item.setOnClickListener {
            if (cantDeleteWhenPopupWindowFlag == 0) { //if flag = 1 its mean popup info menu is open

                AlertDialog.Builder(c)
                    .setTitle("Remove")
                    .setIcon(R.drawable.ic_baseline_warning_24)
                    .setMessage("Are you sure you want to remove the product?")
                    .setPositiveButton("Yes") { dialog, _ ->
                        products.removeAt(position)
                        notifyDataSetChanged()
                        Toast.makeText(c, "Product successfully removed", Toast.LENGTH_SHORT).show()
                        //update totalAmount label at barcode page
                        totalAmountVariable -= totalPriceToShow
                        totalAmountBarcodePage.text = "$totalAmountStr $totalAmountVariable"
                        dialog.dismiss()
                    }
                    .setNegativeButton("No") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            }
        }
        holder.itemView.increaseButton.setOnClickListener {
            // get the number of products in the holder
            productAmount += 1
            holder.itemView.purchase_counter.text = productAmount.toString()

            // get the product price
            totalPriceToShow += productPrice
            holder.itemView.product_price.text =
                totalPriceToShow.toString() //change the total price to show at the current product


            products[position].purchase_counter += 1
            //update totalAmount label at barcode page
            totalAmountVariable += productPrice
            totalAmountBarcodePage.text = "$totalAmountStr $totalAmountVariable"
        }

        holder.itemView.decreaseButton.setOnClickListener {
            // get the number of products in the holder

            // check if still there are products
            if (productAmount > 1) {
                productAmount -= 1
                holder.itemView.purchase_counter.text = productAmount.toString()

                // get the product price
                totalPriceToShow -= productPrice
                holder.itemView.product_price.text =
                    totalPriceToShow.toString() //change the total price to show at the current product


                products[position].purchase_counter -= 1
                //update totalAmount label at barcode page
                totalAmountVariable -= productPrice
                totalAmountBarcodePage.text = "$totalAmountStr $totalAmountVariable"
            }
        }
    }

    override fun getItemCount(): Int = products.size

    class ProductHolder(itemView: View) : ViewHolder(itemView)
}