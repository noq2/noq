package com.example.noq

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface RestApi {
    @GET("/user")
    suspend fun getUsers(): List<FullUserInfo>

    @POST("/user/login")
    fun loginUser(@Body userData: SigningInUserInfo): Call<SigningInUserInfo>

    @POST("/user/register")
    fun registerNewUser(@Body fullUserData: FullUserInfo): Call<FullUserInfo>
}