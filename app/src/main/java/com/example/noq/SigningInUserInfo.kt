package com.example.noq

import com.google.gson.annotations.SerializedName

data class SigningInUserInfo(
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String?
)