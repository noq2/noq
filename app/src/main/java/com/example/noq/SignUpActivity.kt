package com.example.noq

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // hiding keyboard when pressing
        registerButtonId.setOnClickListener { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)

            // calling the function that is responsible for registering new users
            registerFunction()
        }
    }

    /*
        Register new user.
    */
    private fun registerFunction() {
        // get the fields values
        val email = registerEmailValueId.text;
        val pass = registerPasswordValueId.text;
        val confirmPass = registerConfirmPasswordValueId.text;
        val firstName = firstNameValueId.text;
        val lastName = lastNameValueId.text;

        // check if the user inserted all fields
        if (email.toString() == "" || pass.toString() == "" || firstName.toString() == ""
            || lastName.toString() == "" || confirmPass.toString() == ""
        ) {
            AssistingFunctions.printMessage(200, applicationContext, "Details are missing!")
        } else {
            // check if password and confirm password are equal
            if (pass.toString() == confirmPass.toString()) {
                // creating UserInfo object
                val fullUserInfo = FullUserInfo(
                    firstName = firstName.toString(),
                    lastName = lastName.toString(),
                    email = email.toString(),
                    password = pass.toString()
                )

                // register the new user
                val apiService = RestApiService()
                val str = "$firstName $lastName"
                apiService.registerNewUser(str, applicationContext, fullUserInfo) { }
            } else {
                AssistingFunctions.printMessage(
                    200, applicationContext,
                    "Passwords are not matching!"
                )
            }
        }
    }
}