package com.example.noq

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductModel(
    //////////////       0 - type       3 - color                   6 - title             //////////////
    //////////////       1 - subType    4 - price                                        //////////////
    //////////////       2 - size       5 - productSerialNumber                          //////////////
    ////EXM : Shirt,T-Shirt,XL,Black,200,xxxyyy123
    var type: String,
    var subType: String,
    var size: String,
    var color: String,
    var price: Double,
    var productSerialNumber: String,
    var title: String,
    var img: String,
    var purchase_counter: Int
) : Parcelable