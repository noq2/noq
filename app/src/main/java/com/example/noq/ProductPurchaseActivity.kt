package com.example.noq

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.example.noq.AssistingFunctions.totalAmountStr
import kotlinx.android.synthetic.main.activity_product_purchase.*
import java.util.*


class ProductPurchaseActivity : AppCompatActivity() {
    // global error
    var isAllValidFields = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_purchase)

        // get products list from barcode scanner activity
        val productList = intent.getSerializableExtra("productsArrayList")

        // get the total amount
        val totalAmountPrice = intent.getSerializableExtra("totalAmount")
        val str = "$totalAmountStr $totalAmountPrice \n"
        totalAmount.text = str

        // checking fields validation
        checkDate()
        checkCardNumber()
        checkCVV()

        // hiding keyboard when pressing
        paymentButtonId.setOnClickListener { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)

            // calling the function that is responsible for registering new users
            purchaseProducts()
        }
    }

    /*
        The purchase button function.
     */
    private fun purchaseProducts() {
        // all fields are correct
        if (!isAllValidFields) {

        } else {

        }
    }

    /*
        The function checks if date is valid.
     */
    private fun checkDate() {
        MM_YYValueId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                var working = s.toString()
                var isValid = true
                if (working.length == 2 && before == 0) {
                    if (working.toInt() < 1 || working.toInt() > 12) {
                        isValid = false
                    } else {
                        working += "/"
                        MM_YYValueId.setText(working)
                        MM_YYValueId.setSelection(working.length)
                    }
                } else if (working.length == 5 && before == 0) {
                    val enteredYear = working.substring(3)
                    val currentYear: String =
                        Calendar.getInstance().get(Calendar.YEAR).toString().substring(2)
                    if (enteredYear.toInt() < currentYear.toInt()) {
                        isValid = false
                    }
                } else if (working.length != 5) {
                    isValid = false
                }
                if (!isValid) {
                    isAllValidFields = false
                    MM_YYValueId.error = "Enter a valid date: MM/YY"
                } else {
                    MM_YYValueId.error = null
                }
            }
        })
    }

    /*
        The function checks if card number is valid.
     */
    private fun checkCardNumber() {
        cardNumberValueId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                val working = s.toString()
                var isValid = true

                if (working.length != 16) {
                    isValid = false
                }
                if (!isValid) {
                    isAllValidFields = false
                    cardNumberValueId.error = "Enter a valid card number"
                } else {
                    cardNumberValueId.error = null
                }
            }
        })
    }

    /*
        The function checks if cvv number is valid.
     */
    private fun checkCVV() {
        cvvValueId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                val working = s.toString()
                var isValid = true

                if (working.length != 3) {
                    isValid = false
                }
                if (!isValid) {
                    isAllValidFields = false
                    cvvValueId.error = "Enter a valid CVV"
                } else {
                    cvvValueId.error = null
                }
            }
        })
    }
}