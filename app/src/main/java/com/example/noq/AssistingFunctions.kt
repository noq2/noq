package com.example.noq

import android.content.Context
import android.net.ConnectivityManager
import android.view.Gravity
import android.widget.Toast

object AssistingFunctions {
    var totalAmountVariable = 0.0
    const val totalAmountStr = "Total Amount: "

    /*
        The function is printing the correct error message.
     */
    fun printMessage(y_position: Int, applicationContext: Context, message: String) {
        val toast = Toast.makeText(
            applicationContext,
            message, Toast.LENGTH_SHORT
        )
        toast.setGravity(
            Gravity.TOP or Gravity.CENTER_HORIZONTAL,
            0, 200
        )
        toast.setGravity(Gravity.TOP or Gravity.CENTER, 0, y_position)
        toast.show()
    }

    /*
        The function checks if the wifi is connected.
     */
    fun checkNetworkConnection(connMgr: ConnectivityManager): Boolean {
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo?.isConnected() ?: false
    }
}