package com.example.noq

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        // hiding keyboard when pressing
        loginButton.setOnClickListener { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)

            // calling the function that is responsible for logging in users
            signInFunction()
        }
    }

    /*
        Sign In user by his email and password.
     */
    private fun userSignIn(signingInUserInfo: SigningInUserInfo) {
        val apiService = RestApiService()

        if (signingInUserInfo.email == "" || signingInUserInfo.password == "") {
            AssistingFunctions.printMessage(200, applicationContext, "Details are missing!")
        } else {
            val extractedUserName = extractNameFromEmail(signingInUserInfo.email)
            apiService.loginUser(extractedUserName, applicationContext, signingInUserInfo) {}
        }
    }

    /*
        The function for the login button.
    */
    private fun signInFunction() {
        val email = emailValueId.text;
        val pass = passwordValueId.text;

        // creating loginUserInfo object
        val loginUserInfo = SigningInUserInfo(
            email = email.toString(),
            password = pass.toString()
        )

        // check connection status
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val isConnected = AssistingFunctions.checkNetworkConnection(connMgr)

        if (isConnected) {
            // login the user
            userSignIn(loginUserInfo)
        } else {
            // let the user know he is not connected
            AssistingFunctions.printMessage(
                200,
                applicationContext,
                "There is no internet connection!"
            )
        }
    }

    /*
        The function gets an email and extracts a name from it.
    */
    private fun extractNameFromEmail(email: String): String {
        val parts = email.split("@")
        return parts[0]
    }

    /*
        The function redirects to sign up page.
    */
    fun goToSignUpPage(view: View) {
        val intent = Intent(this, SignUpActivity::class.java).apply { }
        startActivity(intent)
    }
}