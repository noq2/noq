package com.example.noq

import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestApiService : ViewModel() {
    private val myResponseList: MutableLiveData<List<FullUserInfo>> = MutableLiveData()

    fun loginUser(
        extractedUserName: String,
        applicationContext: Context,
        userData: SigningInUserInfo,
        onResult: (SigningInUserInfo?) -> Unit
    ) {
        viewModelScope.launch {
            ServiceBuilder.retrofit.loginUser(userData).enqueue(
                object : Callback<SigningInUserInfo> {
                    override fun onFailure(call: Call<SigningInUserInfo>, t: Throwable) {
                        AssistingFunctions.printMessage(
                            200,
                            applicationContext,
                            "Could not sign in!"
                        )

                        onResult(null)
                    }

                    override fun onResponse(
                        call: Call<SigningInUserInfo>,
                        response: Response<SigningInUserInfo>
                    ) {
                        if (response.isSuccessful) {
                            // print message
                            val str = "Hello $extractedUserName!"
                            AssistingFunctions.printMessage(200, applicationContext, str)

                            // logged in successfully - go to barcode scanning page
                            val intent =
                                Intent(
                                    applicationContext,
                                    BarcodeScannerActivity::class.java
                                ).apply { }
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
                            applicationContext.startActivity(intent)

                            val userDetails = response.body()
                            onResult(userDetails)
                        } else {
                            AssistingFunctions.printMessage(
                                200,
                                applicationContext,
                                "Could not sign in!"
                            )
                        }
                    }
                }
            )
        }
    }

    fun registerNewUser(
        fullUserName: String,
        applicationContext: Context,
        fullUserData: FullUserInfo,
        onResult: (FullUserInfo?) -> Unit
    ) {
        viewModelScope.launch {
            ServiceBuilder.retrofit.registerNewUser(fullUserData).enqueue(
                object : Callback<FullUserInfo> {
                    override fun onFailure(call: Call<FullUserInfo>, t: Throwable) {
                        AssistingFunctions.printMessage(
                            200,
                            applicationContext,
                            "Could not sign up!"
                        )
                        onResult(null)
                    }

                    override fun onResponse(
                        call: Call<FullUserInfo>,
                        response: Response<FullUserInfo>
                    ) {
                        if (response.isSuccessful) {
                            val str = "Welcome $fullUserName!"
                            AssistingFunctions.printMessage(200, applicationContext, str)
                            val addedUser = response.body()
                            onResult(addedUser)

                            // registered successfully - go to sign in (main) page
                            val intent =
                                Intent(applicationContext, MainActivity::class.java).apply { }
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
                            applicationContext.startActivity(intent)

                            val userDetails = response.body()
                            onResult(userDetails)
                        } else {
                            AssistingFunctions.printMessage(
                                200,
                                applicationContext,
                                "Could not sign up!"
                            )
                        }
                    }
                }
            )
        }
    }

    fun getUsers() {
        viewModelScope.launch {
            myResponseList.value = ServiceBuilder.retrofit.getUsers()
        }
    }
}