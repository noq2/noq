package com.example.noq

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.noq.AssistingFunctions.totalAmountStr
import com.example.noq.AssistingFunctions.totalAmountVariable
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_barcode_scanner.*


class BarcodeScannerActivity : AppCompatActivity() {
    private lateinit var products: ArrayList<ProductModel>
    private lateinit var adapter: ProductListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode_scanner)

        // set listener for touching everywhere on the screen
        val barcodeLayout =
            findViewById<View>(R.id.barcodeLayout) as RelativeLayout

        //set list & adapter
        products = ArrayList()
        adapter = ProductListAdapter(barcodeLayout, this, products, totalAmountBarcodePage)
        product_recycler.adapter = adapter

        //check
        add_check_products()

        // reset total amount
        val str = "$totalAmountStr $totalAmountVariable"
        totalAmountBarcodePage.text = str
    }

    /*
        The scanning button function.
     */
    fun scan(view: View) {
        val intentIntegrator = IntentIntegrator(this@BarcodeScannerActivity)
        intentIntegrator.setBeepEnabled(false)
        intentIntegrator.setCameraId(0)
        intentIntegrator.setPrompt("SCAN")
        intentIntegrator.setBarcodeImageEnabled(false)
        intentIntegrator.initiateScan()
    }

    /*
        The barcode result functionality.
    */
    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "cancelled", Toast.LENGTH_SHORT).show()
            } else {
                Log.d("BarcodeScannerActivity", "Scanned")
                Toast.makeText(this, "Scanned -> " + result.contents, Toast.LENGTH_SHORT)
                    .show()

                //TODO: NEED TO Check valid input!!
                if (result.contents.contains(",")) {
                    val resultArr = result.contents.split(",").toTypedArray()
                    val hashResult = result.hashCode().toString()

                    //////////////       0 - type       3 - color                   6 - title             //////////////
                    //////////////       1 - subType    4 - price                                        //////////////
                    //////////////       2 - size       5 - productSerialNumber                          //////////////
                    ////EXM : Shirt,T-Shirt,XL,Black,200,xxxyyy123

                    val img = "@drawable/code_android_logo"
                    val newProduct = ProductModel(
                        resultArr[0],
                        resultArr[1],
                        resultArr[2],
                        resultArr[3],
                        resultArr[4].toDouble(),
                        hashResult,
                        resultArr[8],
                        img,
                        1
                    )

                    // add the product to the products list
                    products.add(newProduct)

                    // change total products amount
                    totalAmountVariable += newProduct.price * newProduct.purchase_counter
                    val str = "$totalAmountStr $totalAmountVariable"
                    totalAmountBarcodePage.text = str

                    // notify the adapter that something has changed
                    adapter.notifyDataSetChanged()

                } else {
                    AssistingFunctions.printMessage(
                        200,
                        applicationContext,
                        "Barcode not at the right format!"
                    )
                    Toast.makeText(this, "cancelled", Toast.LENGTH_SHORT).show()
                }

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /*
        The function redirects to purchase page.
    */
    fun goToPurchasePage(view: View) {
        // passing values to purchase activity
        val intent = Intent(this@BarcodeScannerActivity, ProductPurchaseActivity::class.java)
        intent.putExtra("productsArrayList", products)
        intent.putExtra("totalAmount", totalAmountVariable)
        startActivity(intent)
    }

    /** function for check and debug! - delete **/
    fun add_check_products() {
        products.addAll(
            listOf(
                ProductModel(
                    "something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    2
                ),
                ProductModel(
                    "other something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    3
                ),
                ProductModel(
                    "something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    2
                ),
                ProductModel(
                    "other something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    3
                ),
                ProductModel(
                    "something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    1
                ),
                ProductModel(
                    "other something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    2
                ),
                ProductModel(
                    "something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    1
                ),
                ProductModel(
                    "other something",
                    "t-shirt",
                    "XL",
                    "blue",
                    100.0,
                    "xxxx",
                    "xxx",
                    "xxx",
                    1
                )
            )
        )
        for (prod in products) {
            // change total products amount
            totalAmountVariable += prod.price * prod.purchase_counter
        }
        val str = "$totalAmountStr $totalAmountVariable \n"
        totalAmountBarcodePage.text = str
        adapter.notifyDataSetChanged()
    }
}